#!/usr/bin/php
<?PHP

chdir ( '/data/project/listeria' ) ;

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // |E_ALL
ini_set('memory_limit','4000M');

require_once ( '/data/project/listeria/public_html/php/common.php' ) ;
require_once ( '/data/project/listeria/shared.inc' ) ;
require_once ( '/data/project/listeria/public_html/php/wikiquery.php' ) ;
require_once ( '/data/project/listeria/public_html/php/wikidata.php' ) ;


function updateWikis () {
	global $tool_db ;
	$q = 'Q19860885' ;
	$il = new WikidataItemList() ;
	$il->loadItems ( array ( $q ) ) ;

	$o = $il->getItem($q) ;
	foreach ( $o->j->sitelinks AS $wiki => $v ) {
		$page = $v->title ;
#		print "$wiki : $page\n" ;
		$sql = "INSERT IGNORE INTO wikis (name) VALUES ('".$tool_db->real_escape_string($wiki)."')" ;
		if(!$result = $tool_db->query($sql)) die('There was an error running the query [' . $tool_db->error . ']');
	}
}

function getNextWiki () {
	global $tool_db ;
	$ret = '' ;
	$sql = "SELECT * FROM wikis WHERE status='DONE' ORDER BY timestamp LIMIT 1" ;
	if(!$result = $tool_db->query($sql)) die('There was an error running the query [' . $tool_db->error . ']');
	while($o = $result->fetch_object()){
		$ret = $o->name ;
	}
	return $ret ;
}

function getTimestamp() {
	return date ( 'YmdHis' ) ;
}

function setWikiStatus ( $wiki , $status ) {
	global $tool_db ;
	$sql = "UPDATE wikis SET status='".$tool_db->real_escape_string($status)."',timestamp='".getTimestamp()."' WHERE name='".$tool_db->real_escape_string($wiki)."'" ;
	if(!$result = $tool_db->query($sql)) die('There was an error running the query [' . $tool_db->error . ']');
}


$tool_db = openToolDB ( 'listeria_bot' ) ;
$tool_db->set_charset("utf8") ;
updateWikis () ;

if ( count ( $argv ) != 2 ) {
#	print "USAGE: " . $argv[0] . " LANGUAGE\n" ;
	$wiki = getNextWiki() ;
	if ( $wiki == '' ) exit ( 0 ) ; # All running
} else {
	$wiki = strtolower ( trim ( $argv[1] ) ) ;
/*	$lang = preg_replace ( '/wiki$/' , '' , $lang ) ;
	$project = 'wikipedia' ;
	if ( $lang == 'wikidata' ) $project = $lang ;
	if ( $lang == 'commons' ) $project = 'wikimedia' ;
	if ( $lang == 'meta' ) $project = 'wikimedia' ;

	$wiki = "$lang$project" ;
	$wiki = preg_replace ( '/wiki[mp]edia$/' , 'wiki' , $wiki ) ;*/
}

$server_parts = explode ( '.' , getWebserverForWiki ( $wiki ) ) ;
$lang = $server_parts[0] ;
$project = $server_parts[1] ;
if ( $project == 'wikidata' ) $lang = $project ;
#print "$wiki: $lang.$project\n" ;

setWikiStatus ( $wiki , 'RUNNING' ) ;
$sql = "SELECT * FROM wikis WHERE name='$wiki'" ;
if(!$result = $tool_db->query($sql)) die('There was an error running the query [' . $tool_db->error . ']');
while($o = $result->fetch_object()) $wiki_id = $o->id ;
if ( !isset($wiki_id) ) die ( "UNKNOWN WIKI '$wiki' IN DATABASE\n" ) ;
$sql = "DELETE FROM pagestatus WHERE wiki='$wiki_id'" ;
if(!$result = $tool_db->query($sql)) die('There was an error running the query [' . $tool_db->error . ']');

$wq = new WikiQuery ( $lang , $project ) ;
$ns = $wq->get_namespaces() ;


$t1 = 'Wikidata list' ;
$t2 = 'Wikidata list end' ;
$wil = new WikidataItemList($lang) ;
$wil->loadItems ( array ( 'Q19860885' , 'Q19860887' ) ) ;

$sl = $wil->getItem('Q19860885')->getSitelink($wiki) ;
if ( isset($sl) ) $t1 = preg_replace ( '/^[^:]+:/' , '' , $sl ) ;

$sl = $wil->getItem('Q19860887')->getSitelink($wiki) ;
if ( isset($sl) ) $t2 = preg_replace ( '/^[^:]+:/' , '' , $sl ) ;

$t1 = str_replace ( ' ' , '_' , $t1 ) ;
$t2 = str_replace ( ' ' , '_' , $t2 ) ;


$db = openDB ( $lang , $project ) ;

$t1 = $db->real_escape_string ( $t1 ) ;
$t2 = $db->real_escape_string ( $t2 ) ;

$sql = "select page.* from page,templatelinks t1,templatelinks t2 where page_id=t1.tl_from and t1.tl_title='$t1' and page_id=t2.tl_from and t2.tl_title='$t2' and t1.tl_namespace=10 and t2.tl_namespace=10" ;
if ( $wiki == 'dewiki' ) $sql .= " AND page_namespace!=0" ;
$sql .= " ORDER BY rand()" ; // In case of 'broken' pages, don't always process the same ones first
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
while($o = $result->fetch_object()){
	$page = $o->page_title ;
	if ( $o->page_namespace > 0 ) $page = $ns[$o->page_namespace] . ':' . $page ;

	$page_db = $tool_db->real_escape_string($page) ;
	$sql = "REPLACE INTO pagestatus (wiki,page,status,timestamp) VALUES ('$wiki_id','$page_db','RUNNING','".getTimestamp()."')" ;
	if(!$result2 = $tool_db->query($sql)) die('There was an error running the query [' . $tool_db->error . ']');
#	print "$wiki:$page\n" ;
	
	$l = new Listeria ( $lang ) ;
	$success = $l->updatePage ( $page ) ;
	$tool_db = openToolDB ( 'listeria_bot' ) ;
	$tool_db->set_charset("utf8") ;
	$status = isset($l->error) ? $l->error : 'OK' ;
	$sql = "REPLACE INTO pagestatus (wiki,page,status,timestamp) VALUES ('$wiki_id','$page_db','".$tool_db->real_escape_string($status)."','".getTimestamp()."')" ;
	if(!$result2 = $tool_db->query($sql)) die('There was an error running the query [' . $tool_db->error . ']');
}

setWikiStatus ( $wiki , 'DONE' ) ;

?>
