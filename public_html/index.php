<?PHP

ini_set('memory_limit','4000M');
set_time_limit ( 60 * 3 ) ; // Seconds

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR|E_ALL);
ini_set('display_errors', 'On');

require_once ( 'php/common.php' ) ;
require_once ( '../shared.inc' ) ;

//header('Content-type: text/html; charset=UTF-8');
print get_common_header ( '' , 'Listeria' ) ;
print "<div style='float:right'>
<a href='https://commons.wikimedia.org/wiki/File:Listeria_monocytogenes_01.jpg' target='_blank' title='Listeria monocytogenes. Biology pun. Sorry.'>
<img src='https://upload.wikimedia.org/wikipedia/commons/thumb/b/be/Listeria_monocytogenes_01.jpg/160px-Listeria_monocytogenes_01.jpg' border=0 />
</a></div>" ;

function getTimestamp() {
	return date ( 'YmdHis' ) ;
}

$action = get_request ( 'action' , '' ) ;
$testing = isset($_REQUEST['testing']) ;

if ( $action == 'update' ) {
	$project = get_request ( 'project' , '' ) ;
	$lang = get_request ( 'lang' , $project ) ;
	$page = get_request ( 'page' , '' ) ;
	
	print "<p>Trying to update ".htmlentities($page)."...</p>\n" ; myflush() ;
	print "<p>" ;
	
	if ( $project != '' and $project != 'wikipedia' and $project != 'wikidata' ) $l = new Listeria ( $lang , $project ) ;
	else $l = new Listeria ( $lang ) ;
	$l->respectFreq = false ; // Ignore freq setting
	if ( get_request('force',0) ) $l->force_update = 1 ;
	$success = $l->updatePage ( $page ) ;
	
	print "</p>" ;
	print "<p>Return to <a href='https://" . $l->wiki_server . "/wiki/" . myurlencode($page) . "'><b>".htmlentities($page)."</b></a></p>" ;
	print "<p>Also, check out the <a href='./botstatus.php'>current bot status!</a></p>" ;

	$tool_db = openToolDB ( 'listeria_bot' ) ;
	$tool_db->set_charset("utf8") ;
	$status = isset($l->error) ? $l->error : 'OK' ;
	print "<p>Status: $status</p>" ;
	$sql = "SELECT * FROM wikis WHERE name='".$tool_db->real_escape_string($l->wiki)."'" ;
	if(!$result2 = $tool_db->query($sql)) die('There was an error running the query [' . $tool_db->error . ']');
	while($o = $result2->fetch_object()) $wiki_id = $o->id ;
	$page_db = $tool_db->real_escape_string ( str_replace(' ','_',$page) ) ;
	if ( isset($wiki_id) ) {
		$sql = "REPLACE INTO pagestatus (wiki,page,status,timestamp) VALUES ('$wiki_id','$page_db','".$tool_db->real_escape_string($status)."','".getTimestamp()."')" ;
		if(!$result2 = $tool_db->query($sql)) die('There was an error running the query [' . $tool_db->error . ']');
	}

} else if ( $action == 'get_wikitext' ) {
	$lang = get_request ( 'lang' , '' ) ;
	$page = get_request ( 'page' , '' ) ;
	$l = new Listeria ( $lang ) ;
	$l->loadPage ( $page ) ;
	$w = $l->getWikiText() ;
	print "<pre>".htmlentities($w)."</pre>" ;

} else {
	print "<div class='lead'><a href='https://www.wikidata.org/wiki/User:ListeriaBot'>This bot</a> can generate and update lists on Wikipedia, based on <a href='https://wdq.wmflabs.org'>WDQ</a> queries.<br/>" ;
	print "Check out <a href='http://magnusmanske.de/wordpress/?p=301'>my blog post</a> explaining how this works.</div>" ;
}

print get_common_footer() ;

?>